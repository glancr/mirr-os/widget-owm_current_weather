# frozen_string_literal: true

Rails.application.routes.draw do
  mount OwmCurrentWeather::Engine => '/owm_current_weather'
end
